package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class Person  {
  
  private String birthDate = null;
  private String gender = null;
  private Integer age = null;
  private String occupationCode = null;

  
  /**
   * Date of Birth
   **/
  @ApiModelProperty(value = "Date of Birth")
  @JsonProperty("birthDate")
  public String getBirthDate() {
    return birthDate;
  }
  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  
  /**
   * gender of the insured person
   **/
  @ApiModelProperty(value = "gender of the insured person")
  @JsonProperty("gender")
  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }

  
  /**
   * current age of the insured person
   **/
  @ApiModelProperty(value = "current age of the insured person")
  @JsonProperty("age")
  public Integer getAge() {
    return age;
  }
  public void setAge(Integer age) {
    this.age = age;
  }

  
  /**
   * based on occupetion code list.
   **/
  @ApiModelProperty(value = "based on occupetion code list.")
  @JsonProperty("occupationCode")
  public String getOccupationCode() {
    return occupationCode;
  }
  public void setOccupationCode(String occupationCode) {
    this.occupationCode = occupationCode;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Person {");
    
    sb.append("  birthDate: ").append(birthDate).append("");
    sb.append("  gender: ").append(gender).append("");
    sb.append("  age: ").append(age).append("");
    sb.append("  occupationCode: ").append(occupationCode).append("");
    sb.append("}");
    return sb.toString();
  }
}
