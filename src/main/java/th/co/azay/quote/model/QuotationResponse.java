package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import th.co.azay.quote.model.Premium;
import th.co.azay.quote.model.QuoteItem;


@ApiModel(description = "")
public class QuotationResponse extends CommonResponse {
  
  private String quotationNumber = null;
  private List<QuoteItem> quoteItems = new ArrayList<QuoteItem>();
  private Premium totalPremium = null;

  
  /**
   * unique quote number
   **/
  @ApiModelProperty(value = "unique quote number")
  @JsonProperty("quotationNumber")
  public String getQuotationNumber() {
    return quotationNumber;
  }
  public void setQuotationNumber(String quotationNumber) {
    this.quotationNumber = quotationNumber;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("quoteItems")
  public List<QuoteItem> getQuoteItems() {
    return quoteItems;
  }
  public void setQuoteItems(List<QuoteItem> quoteItems) {
    this.quoteItems = quoteItems;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("totalPremium")
  public Premium getTotalPremium() {
    return totalPremium;
  }
  public void setTotalPremium(Premium totalPremium) {
    this.totalPremium = totalPremium;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuotationResponse {");
    
    sb.append("  quotationNumber: ").append(quotationNumber).append("");
    sb.append("  quoteItems: ").append(quoteItems).append("");
    sb.append("  totalPremium: ").append(totalPremium).append("");
    sb.append("}");
    return sb.toString();
  }
}
