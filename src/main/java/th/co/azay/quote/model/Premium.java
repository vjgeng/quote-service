package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import th.co.azay.quote.model.AdditionalCosts;
import th.co.azay.quote.model.Discount;


@ApiModel(description = "")
public class Premium  {
  
  private BigDecimal grossPremium = null;
  private BigDecimal netPremium = null;
  private List<AdditionalCosts> additionalCosts = new ArrayList<AdditionalCosts>();
  private List<Discount> discounts = new ArrayList<Discount>();
  private String currency = null;

  
  /**
   * gross premium
   **/
  @ApiModelProperty(value = "gross premium")
  @JsonProperty("grossPremium")
  public BigDecimal getGrossPremium() {
    return grossPremium;
  }
  public void setGrossPremium(BigDecimal grossPremium) {
    this.grossPremium = grossPremium;
  }

  
  /**
   * net premium
   **/
  @ApiModelProperty(value = "net premium")
  @JsonProperty("netPremium")
  public BigDecimal getNetPremium() {
    return netPremium;
  }
  public void setNetPremium(BigDecimal netPremium) {
    this.netPremium = netPremium;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("additionalCosts")
  public List<AdditionalCosts> getAdditionalCosts() {
    return additionalCosts;
  }
  public void setAdditionalCosts(List<AdditionalCosts> additionalCosts) {
    this.additionalCosts = additionalCosts;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("discounts")
  public List<Discount> getDiscounts() {
    return discounts;
  }
  public void setDiscounts(List<Discount> discounts) {
    this.discounts = discounts;
  }

  
  /**
   * fixed to THB
   **/
  @ApiModelProperty(value = "fixed to THB")
  @JsonProperty("currency")
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Premium {");
    
    sb.append("  grossPremium: ").append(grossPremium).append("");
    sb.append("  netPremium: ").append(netPremium).append("");
    sb.append("  additionalCosts: ").append(additionalCosts).append("");
    sb.append("  discounts: ").append(discounts).append("");
    sb.append("  currency: ").append(currency).append("");
    sb.append("}");
    return sb.toString();
  }
}
