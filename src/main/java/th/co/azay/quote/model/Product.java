package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@ApiModel(description = "")
public class Product  {
  
  private String code = null;
  private List<String> selectedCoverages = new ArrayList<String>();
  private String coverageType = null;
  private Integer sumAssured = null;
  private Integer benefitTerm = null;
  private Integer premiumTerm = null;
  private String paymentFrequency = null;
  private BigDecimal discountAmount = null;
  private Integer discountRation = null;

  
  /**
   * Base Product code, derived from list.
   **/
  @ApiModelProperty(required = true, value = "Base Product code, derived from list.")
  @JsonProperty("code")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  
  /**
   * Main cover code, derived from list.
   **/
  @ApiModelProperty(value = "Main cover code, derived from list.")
  @JsonProperty("selectedCoverages")
  public List<String> getSelectedCoverages() {
    return selectedCoverages;
  }
  public void setSelectedCoverages(List<String> selectedCoverages) {
    this.selectedCoverages = selectedCoverages;
  }

  
  /**
   * Coverage Type, BASIC, RIDER
   **/
  @ApiModelProperty(value = "Coverage Type, BASIC, RIDER")
  @JsonProperty("coverageType")
  public String getCoverageType() {
    return coverageType;
  }
  public void setCoverageType(String coverageType) {
    this.coverageType = coverageType;
  }

  
  /**
   * sum Assured, how much return you want when exit the insurence
   **/
  @ApiModelProperty(value = "sum Assured, how much return you want when exit the insurence")
  @JsonProperty("sumAssured")
  public Integer getSumAssured() {
    return sumAssured;
  }
  public void setSumAssured(Integer sumAssured) {
    this.sumAssured = sumAssured;
  }

  
  /**
   * how long is the benefit term
   **/
  @ApiModelProperty(value = "how long is the benefit term")
  @JsonProperty("benefitTerm")
  public Integer getBenefitTerm() {
    return benefitTerm;
  }
  public void setBenefitTerm(Integer benefitTerm) {
    this.benefitTerm = benefitTerm;
  }

  
  /**
   * how long is the benefit  term
   **/
  @ApiModelProperty(value = "how long is the benefit  term")
  @JsonProperty("premiumTerm")
  public Integer getPremiumTerm() {
    return premiumTerm;
  }
  public void setPremiumTerm(Integer premiumTerm) {
    this.premiumTerm = premiumTerm;
  }

  
  /**
   * pay the premium monthly, yearly, weekly, dayli, derived from list.
   **/
  @ApiModelProperty(value = "pay the premium monthly, yearly, weekly, dayli, derived from list.")
  @JsonProperty("paymentFrequency")
  public String getPaymentFrequency() {
    return paymentFrequency;
  }
  public void setPaymentFrequency(String paymentFrequency) {
    this.paymentFrequency = paymentFrequency;
  }

  
  /**
   * TBD
   **/
  @ApiModelProperty(value = "TBD")
  @JsonProperty("discountAmount")
  public BigDecimal getDiscountAmount() {
    return discountAmount;
  }
  public void setDiscountAmount(BigDecimal discountAmount) {
    this.discountAmount = discountAmount;
  }

  
  /**
   * TBD
   **/
  @ApiModelProperty(value = "TBD")
  @JsonProperty("discountRation")
  public Integer getDiscountRation() {
    return discountRation;
  }
  public void setDiscountRation(Integer discountRation) {
    this.discountRation = discountRation;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Product {");
    
    sb.append("  code: ").append(code).append("");
    sb.append("  selectedCoverages: ").append(selectedCoverages).append("");
    sb.append("  coverageType: ").append(coverageType).append("");
    sb.append("  sumAssured: ").append(sumAssured).append("");
    sb.append("  benefitTerm: ").append(benefitTerm).append("");
    sb.append("  premiumTerm: ").append(premiumTerm).append("");
    sb.append("  paymentFrequency: ").append(paymentFrequency).append("");
    sb.append("  discountAmount: ").append(discountAmount).append("");
    sb.append("  discountRation: ").append(discountRation).append("");
    sb.append("}");
    return sb.toString();
  }
}
