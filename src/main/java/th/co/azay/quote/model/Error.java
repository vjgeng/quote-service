package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import th.co.azay.quote.model.ErrorDetail;


@ApiModel(description = "")
public class Error  {
  
  private List<ErrorDetail> errors = new ArrayList<ErrorDetail>();

  
  /**
   * List of Error Information
   **/
  @ApiModelProperty(value = "List of Error Information")
  @JsonProperty("errors")
  public List<ErrorDetail> getErrors() {
    return errors;
  }
  public void setErrors(List<ErrorDetail> errors) {
    this.errors = errors;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {");
    
    sb.append("  errors: ").append(errors).append("");
    sb.append("}");
    return sb.toString();
  }
}
