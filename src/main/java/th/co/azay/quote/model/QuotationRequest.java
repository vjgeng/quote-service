package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import th.co.azay.quote.model.Person;
import th.co.azay.quote.model.Product;


@ApiModel(description = "")
public class QuotationRequest  {
  
  private String salesChannel = null;
  private Person insured = null;
  private Person payer = null;
  private List<Product> products = new ArrayList<Product>();

  
  /**
   * specified channel agency, bancassurance, drtv
   **/
  @ApiModelProperty(value = "specified channel agency, bancassurance, drtv")
  @JsonProperty("salesChannel")
  public String getSalesChannel() {
    return salesChannel;
  }
  public void setSalesChannel(String salesChannel) {
    this.salesChannel = salesChannel;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("insured")
  public Person getInsured() {
    return insured;
  }
  public void setInsured(Person insured) {
    this.insured = insured;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("payer")
  public Person getPayer() {
    return payer;
  }
  public void setPayer(Person payer) {
    this.payer = payer;
  }

  
  /**
   **/
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("products")
  public List<Product> getProducts() {
    return products;
  }
  public void setProducts(List<Product> products) {
    this.products = products;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuotationRequest {");
    
    sb.append("  salesChannel: ").append(salesChannel).append("");
    sb.append("  insured: ").append(insured).append("");
    sb.append("  payer: ").append(payer).append("");
    sb.append("  products: ").append(products).append("");
    sb.append("}");
    return sb.toString();
  }
}
