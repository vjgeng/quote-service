package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class ErrorDetail  {
  
  private String code = null;
  private String message = null;
  private String severityLevel = null;

  
  /**
   * The error code associated with the invalid request.
   **/
  @ApiModelProperty(value = "The error code associated with the invalid request.")
  @JsonProperty("code")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  
  /**
   * The error message associated with the invalid request
   **/
  @ApiModelProperty(value = "The error message associated with the invalid request")
  @JsonProperty("message")
  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }

  
  /**
   * SeverityLevel Code (\"ERROR\", \"WARNING\", \"INFO\")
   **/
  @ApiModelProperty(value = "SeverityLevel Code (\"ERROR\", \"WARNING\", \"INFO\")")
  @JsonProperty("severityLevel")
  public String getSeverityLevel() {
    return severityLevel;
  }
  public void setSeverityLevel(String severityLevel) {
    this.severityLevel = severityLevel;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorDetail {");
    
    sb.append("  code: ").append(code).append("");
    sb.append("  message: ").append(message).append("");
    sb.append("  severityLevel: ").append(severityLevel).append("");
    sb.append("}");
    return sb.toString();
  }
}
