package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import th.co.azay.quote.model.Premium;
import th.co.azay.quote.model.Product;


@ApiModel(description = "")
public class QuoteItem  {
  
  private Product product = null;
  private Premium annualPremium = null;
  private Premium monthlyPremium = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("product")
  public Product getProduct() {
    return product;
  }
  public void setProduct(Product product) {
    this.product = product;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("annualPremium")
  public Premium getAnnualPremium() {
    return annualPremium;
  }
  public void setAnnualPremium(Premium annualPremium) {
    this.annualPremium = annualPremium;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("monthlyPremium")
  public Premium getMonthlyPremium() {
    return monthlyPremium;
  }
  public void setMonthlyPremium(Premium monthlyPremium) {
    this.monthlyPremium = monthlyPremium;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class QuoteItem {");
    
    sb.append("  product: ").append(product).append("");
    sb.append("  annualPremium: ").append(annualPremium).append("");
    sb.append("  monthlyPremium: ").append(monthlyPremium).append("");
    sb.append("}");
    return sb.toString();
  }
}
