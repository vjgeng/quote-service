package th.co.azay.quote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;


@ApiModel(description = "")
public class Discount  {
  
  private String code = null;
  private String type = null;
  private String name = null;
  private BigDecimal amount = null;
  private BigDecimal rate = null;
  private Boolean recurring = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("code")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("type")
  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("amount")
  public BigDecimal getAmount() {
    return amount;
  }
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("rate")
  public BigDecimal getRate() {
    return rate;
  }
  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("recurring")
  public Boolean getRecurring() {
    return recurring;
  }
  public void setRecurring(Boolean recurring) {
    this.recurring = recurring;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Discount {");
    
    sb.append("  code: ").append(code).append("");
    sb.append("  type: ").append(type).append("");
    sb.append("  name: ").append(name).append("");
    sb.append("  amount: ").append(amount).append("");
    sb.append("  rate: ").append(rate).append("");
    sb.append("  recurring: ").append(recurring).append("");
    sb.append("}");
    return sb.toString();
  }
}
