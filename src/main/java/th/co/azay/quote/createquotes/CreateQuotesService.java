package th.co.azay.quote.createquotes;

import th.co.azay.quote.model.QuotationRequest;
import th.co.azay.quote.model.QuotationResponse;
import th.co.azay.quote.onlinefastquote.CreateQuoteException;

public interface CreateQuotesService {
    QuotationResponse createQuote(String systemName, QuotationRequest quote) throws CreateQuoteException;
    QuotationResponse updateQuote(QuotationRequest quote);
    QuotationResponse getQuote(String quoteId);

}
