package th.co.azay.quote.createquotes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import th.co.azay.quote.model.Premium;
import th.co.azay.quote.model.QuotationRequest;
import th.co.azay.quote.model.QuotationResponse;
import th.co.azay.quote.onlinefastquote.CreateQuoteException;
import th.co.azay.quote.onlinefastquote.OnlineFastQuoteService;
import th.co.azay.quote.onlinefastquote.dtos.Gender;
import th.co.azay.quote.onlinefastquote.dtos.SolutionDTO;
import th.co.azay.quote.onlinefastquote.dtos.SolutionWrapperDTO;
import th.co.azay.quote.onlinefastquote.dtos.findproduct.ProductDetail;

import java.time.LocalDate;

@Service("createQuotesService")
public class CreateQuotesServiceImpl implements CreateQuotesService{

    @Value("${onlinefq.url.findproduct}")
    private String FIND_PRODUCT_URL;

    @Value("${onlinefq.url.createsolution}")
    private String CREATE_SOLUTION_URL;

    @Value("${onlinefq.url.calculatewithriderandadditional}")
    private String CALCULATE_PREMIUM_URL;

    private final String CHANNEL_ONLINE = "online";
    private final String OE = "TH";
    private final String FORM = "685";

    @Autowired
    OnlineFastQuoteService onlineFastQuoteService;

    @Override
    public QuotationResponse createQuote(String systemName, QuotationRequest quote) throws CreateQuoteException {
        QuotationResponse quotationResponse = null;

        this.validateQuotationRequest(quote);
        ProductDetail product = this.findProdcut(quote);
        SolutionDTO solution = this.createSolution(quote);
        this.setupSolution(quote, product, solution);
        SolutionWrapperDTO baseSolution = this.calculatePremiumcalculatePremium(solution);
        quotationResponse = this.mapQuotationResponse(baseSolution);

        return quotationResponse;
    }

    private ProductDetail findProdcut(QuotationRequest quote) throws CreateQuoteException{
        return onlineFastQuoteService.findProduct(FIND_PRODUCT_URL, quote.getProducts().get(0).getCode());
    }

    private SolutionDTO createSolution(QuotationRequest quote) throws CreateQuoteException{
        return onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, CHANNEL_ONLINE, OE, FORM, quote.getProducts().get(0).getCode());
    }

    private SolutionWrapperDTO calculatePremiumcalculatePremium(SolutionDTO solution) throws CreateQuoteException{
        return onlineFastQuoteService.calculatePremium(CALCULATE_PREMIUM_URL, CHANNEL_ONLINE, OE, FORM, solution);
    }

    private void setupSolution(QuotationRequest quote, ProductDetail product, SolutionDTO solution) throws CreateQuoteException{
        solution.setPackagesName(quote.getProducts().get(0).getCode());

        Gender gender = ("M".equalsIgnoreCase(quote.getInsured().getGender()))?Gender.M:Gender.F;
        solution.getPersons().get("INSURED").setGender(gender);
        solution.getPersons().get("INSURED").setDateOfBirth(LocalDate.now().plusYears(-1 * quote.getInsured().getAge()));

        String occupationCode = ("n/a".equalsIgnoreCase(product.getRules().get(0).getOccupation()))?"A10101":product.getRules().get(0).getOccupation();
        solution.getPersons().get("INSURED").getOccupation().setCode(occupationCode);
    }

    private QuotationResponse mapQuotationResponse(SolutionWrapperDTO baseSolution) throws CreateQuoteException {
        QuotationResponse quotationResponse = null;

        if (baseSolution != null && baseSolution.getTotalPremiumAnnual() != null){
            quotationResponse = new QuotationResponse();
            Premium premium = new Premium();
            premium.setNetPremium(baseSolution.getTotalPremiumAnnual());
            quotationResponse.setTotalPremium(premium);

        }else{
            throw new CreateQuoteException("Cannot calculate premium.");
        }

        return quotationResponse;
    }

    private void validateQuotationRequest(QuotationRequest quote) throws CreateQuoteException {
        if (quote == null) throw new CreateQuoteException("QuotationRequest is null");
        if (quote.getProducts().size() == 0) throw new CreateQuoteException("Please provide product");
        if (quote.getProducts().get(0) == null) throw new CreateQuoteException("QuotationRequest is null");
        if (quote.getProducts().get(0).getCode() == null) throw new CreateQuoteException("Package is null");
        if (quote.getInsured() == null) throw new CreateQuoteException("Insured is null, Please specify insured");
        if (quote.getInsured().getGender() == null) throw new CreateQuoteException("Gender is null, Please provide gender");
        if (quote.getInsured().getAge() == null ||quote.getInsured().getAge() == 0) throw new CreateQuoteException("Age is null or 0, Please provide age");
    }

    @Override
    public QuotationResponse updateQuote(QuotationRequest quote) {
        return null;
    }

    @Override
    public QuotationResponse getQuote(String quoteId) {
        return null;
    }
}
