package th.co.azay.quote.createquotes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import th.co.azay.quote.api.QuotesApi;
import th.co.azay.quote.model.*;
import th.co.azay.quote.onlinefastquote.CreateQuoteException;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CreateQuotesController implements QuotesApi {
    @Autowired
    CreateQuotesService createQuotesService;

    @Override
    //public ResponseEntity<QuotationResponse> createQuote(String authorization, String date, String xRequestID, String xApplicationName, QuotationRequest quote) {
    public ResponseEntity<QuotationResponse> createQuote(String xApplicationName, QuotationRequest quote) {
        List<String> errors = new ArrayList<String>();
        QuotationResponse quotationResponse = null;
        try {
            quotationResponse = createQuotesService.createQuote(xApplicationName, quote);
        } catch (CreateQuoteException e) {
            quotationResponse = new QuotationResponse();

            errors.add(e.getMessage());
            quotationResponse.setErrors(errors);
            return new ResponseEntity(quotationResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok(quotationResponse);
    }

    @Override
    public ResponseEntity<QuotationResponse> getQuote(String authorization, String date, String xRequestID, String xApplicationName, String quoteId) {
        return null;
    }

    @Override
    public ResponseEntity<QuotationResponse> updateQuote(String authorization, String date, String xRequestID, String xApplicationName, QuotationRequest quote) {
        return null;
    }
}
