package th.co.azay;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import th.co.azay.quote.createquotes.CreateQuotesService;
import th.co.azay.quote.model.Person;
import th.co.azay.quote.model.Product;
import th.co.azay.quote.model.QuotationRequest;
import th.co.azay.quote.model.QuotationResponse;
import th.co.azay.quote.onlinefastquote.CreateQuoteException;
import th.co.azay.quote.onlinefastquote.OnlineFastQuoteService;
import th.co.azay.quote.onlinefastquote.OnlineFastQuoteServiceImpl;
import th.co.azay.quote.onlinefastquote.dtos.Gender;
import th.co.azay.quote.onlinefastquote.dtos.SolutionDTO;
import th.co.azay.quote.onlinefastquote.dtos.SolutionWrapperDTO;
import th.co.azay.quote.onlinefastquote.dtos.findproduct.ProductDetail;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class QuoteServiceApplicationTests {
	private static final String FIND_PRODUCT_URL = "https://api-test.allianz.com/public-module-qa/services/rest/findProduct"; //productCode=HEALTH_KIDS
	private static final String CREATE_SOLUTION_URL = "https://api-test.allianz.com/public-module-dev/services/rest/solutions/riders";
	private static final String CALCULATE_PREMIUM_URL = "https://api-test.allianz.com/public-module-dev/services/rest/solutions/calculateWithRiderAndAdditionalRider";

	@Autowired
	MockMvc mvc;

	@Autowired
	OnlineFastQuoteService onlineFastQuoteService;

	@Autowired
	CreateQuotesService createQuotesService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void calculatePremiumHEALTH_KIDSMale2Success() throws Exception{
		ProductDetail product = onlineFastQuoteService.findProduct(FIND_PRODUCT_URL, "HEALTH_KIDS");
		SolutionDTO solution = onlineFastQuoteService.createSolution(CREATE_SOLUTION_URL, "online", "TH", "685", "HEALTH_KIDS");
		solution.setPackagesName("HEALTH_KIDS");
		solution.getPersons().get("INSURED").setGender(Gender.M);
		solution.getPersons().get("INSURED").setDateOfBirth(LocalDate.now().plusYears(-2));

		String occupationCode = ("n/a".equalsIgnoreCase(product.getRules().get(0).getOccupation()))?"A10101":product.getRules().get(0).getOccupation();
		solution.getPersons().get("INSURED").getOccupation().setCode(occupationCode);
		//solution.getModules().get("MWLA9021").setSelected(true);
		//solution.getModules().get("CI48").setSelected(true);
		//solution.getModules().get("CBN").setSelected(true);
		//solution.getModules().get("HSS").setSelected(true);
		SolutionWrapperDTO baseSolution = onlineFastQuoteService.calculatePremium(CALCULATE_PREMIUM_URL, "online", "TH", "685", solution);

		Assert.assertNotNull(baseSolution);
		assertEquals(baseSolution.getTotalPremiumAnnual(), new BigDecimal("40576"));

	}

	@Test
	public void createQuoteSuccess() throws Exception {
		QuotationRequest quotationRequest = new QuotationRequest();
		QuotationResponse quotationResponse = null;

		List<Product> products = new ArrayList<Product>();
		Product product = new Product();
		product.setCode("HEALTH_RETIRE");
		products.add(product);

		quotationRequest.setProducts(products);

		Person insured = new Person();
		insured.setGender("M");
		insured.setAge(40);

		quotationRequest.setInsured(insured);
		quotationResponse = createQuotesService.createQuote("onlinefastQuote",  quotationRequest);
		createQuotesService.createQuote("onlinefastQuote",  quotationRequest);

		Assert.assertNotNull(quotationResponse);
		assertEquals(quotationResponse.getTotalPremium().getNetPremium(), new BigDecimal(27548));
	}

	@Test
	public void createQuoteNoPayloadReturn400() throws Exception {
		String uri = "/v1/api/quotes";

		String inputJson = "";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).header("X-Application-Name", "onlinefq").content(inputJson)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
	}

	@Test
	public void createQuoteNoProductReturn500() throws Exception {
		String uri = "/v1/api/quotes";

		String inputJson = "{}";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).header("X-Application-Name", "onlinefq").content(inputJson)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(500, status);
	}

	@Test
	public void createQuoteNoGenderAndAgeReturn500() throws Exception {
		String uri = "/v1/api/quotes";

		String inputJson = "{ \"products\": [ { \"code\": \"HEALTH_RETIRE\" } ] }";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).header("X-Application-Name", "onlinefq").content(inputJson)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(500, status);
	}

	@Test
	public void createQuoteSuccessReturn200() throws Exception {
		String uri = "/v1/api/quotes";

		String inputJson = "{ \"insured\": { \"gender\": \"M\",\"age\": 40 }, \"products\": [ { \"code\": \"HEALTH_RETIRE\" } ] }";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).header("X-Application-Name", "onlinefq").content(inputJson)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
